export const dateNow = (): string => {
  const date = new Date();
  const dateNow =
    [
      date.getFullYear(),
      (date.getMonth() + 1).toString().padStart(2, '0'),
      date.getDate().toString().padStart(2, '0'),
    ].join('-') +
    ' ' +
    [
      date.getHours().toString().padStart(2, '0'),
      date.getMinutes().toString().padStart(2, '0'),
      date.getSeconds().toString().padStart(2, '0'),
    ].join(':');

  return dateNow;
};

export const getRandomFilxdInteger = (length): number => {
  const randomNumber = Math.floor(
    Math.pow(10, length - 1) +
      Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1),
  );

  return randomNumber;
};

export const getRandomString = (length): string => {
  const randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let result: string = '';
  for (let i = 0; i < length; i++) {
    result += randomChars.charAt(
      Math.floor(Math.random() * randomChars.length),
    );
  }
  return result.toUpperCase();
};

export const getformatMobileNumberToSMS = (number_tel): string => {
  let result = number_tel
    .replace(/^\0/, '66')
    .replace(/-/g, '')
    .replace(/\s/g, '');
  return result;
};

export const getRawSql = (query) => {
  let [sql, params] = query.getQueryAndParameters();
  params.forEach((value) => {
    if (typeof value === 'string') {
      sql = sql.replace('?', `"${value}"`);
    }
    if (typeof value === 'object') {
      if (Array.isArray(value)) {
        sql = sql.replace(
          '?',
          value
            .map((element) =>
              typeof element === 'string' ? `"${element}"` : element,
            )
            .join(','),
        );
      } else {
        sql = sql.replace('?', value);
      }
    }
    if (['number', 'boolean'].includes(typeof value)) {
      sql = sql.replace('?', value.toString());
    }
  });

  console.log(sql);
};
