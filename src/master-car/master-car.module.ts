import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TCarSeriesEntity } from 'src/entities/tcarseries.entity';
import { TCarTypeEntity } from 'src/entities/tcartype.entity';
import { TcarModelEntity } from 'src/entities/tcarmodel.entity';
import { MasterCarController } from './master-car.controller';
import { MasterCarService } from './master-car.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      TcarModelEntity,
      TCarSeriesEntity,
      TCarTypeEntity,
    ]),
  ],
  controllers: [MasterCarController],
  providers: [MasterCarService],
})
export class MasterCarModule {}
