import { HttpStatus, Injectable, Param } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { searchCarSeries } from 'src/dto/tcarseries.dto';
import { TCarSeriesEntity } from 'src/entities/tcarseries.entity';
import { TCarTypeEntity } from 'src/entities/tcartype.entity';
import { TcarModelEntity } from 'src/entities/tcarmodel.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MasterCarService {
  constructor(
    @InjectRepository(TcarModelEntity)
    private TcarModelEntity: Repository<TcarModelEntity>,

    @InjectRepository(TCarSeriesEntity)
    private tCarSeriesEntity: Repository<TCarSeriesEntity>,

    @InjectRepository(TCarTypeEntity)
    private tCarTypeEntity: Repository<TCarTypeEntity>,
  ) {}

  async getCarType() {
    try {
      const carType = await this.tCarTypeEntity.find({
        select: ['car_type_id', 'title', 'isactive'],
        where: {
          isactive: 'Y',
        },
      });
      if (!carType) {
        return null;
      }
      return carType;
    } catch (e) {
      return e.message;
    }
  }

  async getCarModel(id: number) {
    try {
      console.log('id', id);
      const carModel = await this.TcarModelEntity.find({
        select: ['car_model_id', 'car_type_id', 'title', 'isactive'],
        where: {
          car_type_id: id,
          isactive: 'Y',
        },
      });
      if (carModel) {
        return carModel;
      }

      return null;
    } catch (e) {
      return e.message;
    }
  }

  async getCarSeries(@Param('id') id: number) {
    try {
      const carSeries = await this.tCarSeriesEntity.find({
        select: [
          'car_series_id',
          'car_model_id',
          'car_type_id',
          'title',
          'model',
          'car_number',
          'engine_number',
          'cc',
          'isactive',
          'isactive',
        ],
        where: {
          car_model_id: id,
          isactive: 'Y',
        },
      });
      if (carSeries) {
        return carSeries;
      }
    } catch (e) {
      return e.message;
    }
  }
}
