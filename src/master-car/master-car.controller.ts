import { Body, Controller, Get, Param, UseGuards } from '@nestjs/common';
import { MasterCarService } from './master-car.service';

@Controller('master-car')
export class MasterCarController {
    constructor(
        private readonly masterCarService : MasterCarService
    ) {}

    @Get('get-brand')
    async getBrand() {
        const result = await this.masterCarService.getCarType();
        return result;
    }

    @Get('get-models/:id')
    async getModel(@Param("id") id: number) {
        
        const result = await this.masterCarService.getCarModel(id);
        return result;
    }

    @Get('get-series/:id')
    async getSeries(@Param("id") id: number) {
        
        const result = await this.masterCarService.getCarSeries(id);
        return result;
    }
}
