import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    Index,
  } from 'typeorm';
  
  import * as helpers from 'src/Helpers/helpers';
  
  @Entity({ name: 't_car_series' })
  export class TCarSeriesEntity {
    @Index('idx_car_series_id')
    @PrimaryGeneratedColumn()
    car_series_id: number;
    
    @Column({
        type: 'double',
    })
    margin: string;

    @Column({
        type: 'date',
    })
    margin_date: string;

    @Column({
        type: 'tinyint',
    })
    goa: string;

    @Index('idx_car_model_model')
    @Column({
        type: 'int',
    })
    car_model_id: number;
    
    @Index('idx_car_typ_eid')
    @Column({
        type: 'int',
    })
    car_type_id: number;

    @Index('idx_car_type')
    @Column({
        type: 'tinyint',
    })
    car_type: string;

    @Column({
        type: 'varchar',
        length : 100
    })
    title: string;

    @Column({
        type: 'varchar',
        length : 100
    })
    model: string;

    @Column({
        type: 'double',
    })
    retail: string;

    @Column({
        type: 'double',
    })
    price: string;

    @Column({
        type: 'varchar',
        length : 7
    })
    car_number: string;

    @Column({
        type: 'varchar',
        length : 4
    })
    engine_number: string;

    @Column({
        type: 'int',
    })
    rank: number;

    @Column({
        type: 'date',
    })
    expire: string;

    @Column({
        type: 'date',
    })
    start_date: string;

    @Column({
        type: 'varchar',
        length : 10
    })
    cc: string;

    @Column({
        type: 'varchar',
        length : 10
    })
    gear: string;

    @Column({
        type: 'tinyint',
    })
    navigator: string;

    @Column({
        type: 'char',
        length: 1,
    })
    isactive: string;
    
    @Column({
      type: 'datetime',
    })
    created_at: string;
  
    @Column({
      type: 'int',
    })
    created_by: number;
  
    @Column({
      type: 'datetime',
    })
    updated_at: string;
  
    @Column({
      type: 'int',
    })
    updated_by: number;
  
  }