import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    Index,
    OneToOne,
    JoinColumn,
  } from 'typeorm';
  
  import * as helpers from 'src/Helpers/helpers';
  import { TCarTypeEntity } from './tcartype.entity';
  
  @Entity({ name: 't_car_model' })
  export class TcarModelEntity {
    @Index('idx_car_model_id')
    @PrimaryGeneratedColumn()
    car_model_id: number;
    
  
    @Index('idx_car_type_id')
    @Column({
        type: 'int',
    })
    car_type_id: number;
    
    @Column({
      type: 'varchar',
      length: 10,
    })
    sale_model_01: string;
    
    @Column({
        type: 'varchar',
        length: 100,
    })
    title: string;

    @Column({
        type: 'int',
    })
    rank: string;

    @Column({
        type: 'date',
    })
    expire: string;

    @Column({
        type: 'varchar',
        length: 3,
    })
    acc03: string;

    @Column({
        type: 'varchar',
        length: 255,
    })
    insure_name: string;

    @Column({
        type: 'char',
        length: 1,
    })
    isactive: string;
    
    @Column({
      type: 'datetime',
    })
    created_at: string;
  
    @Column({
      type: 'int',
    })
    created_by: number;
  
    @Column({
      type: 'datetime',
    })
    updated_at: string;
  
    @Column({
      type: 'int',
    })
    updated_by: number;

   
    @OneToOne(() => TCarTypeEntity)
    @JoinColumn({"name":"car_type_id"})
    tCarType: TCarTypeEntity;
    
  }