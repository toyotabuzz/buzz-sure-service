import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    Index,
    OneToOne,
    JoinColumn,
  } from 'typeorm';
  
  import * as helpers from 'src/Helpers/helpers';
  
  @Entity({ name: 't_car_type' })
  export class TCarTypeEntity {
    @Index('idx_car_type_id')
    @PrimaryGeneratedColumn()
    car_type_id: number;
    
    @Index('idx_title')
    @Column({
        type: 'varchar',
        length : 255
    })
    title: string;

    @Column({
        type: 'char',
        length: 1,
    })
    isactive: string;
    
    @Column({
      type: 'datetime',
    })
    created_at: string;
  
    @Column({
      type: 'int',
    })
    created_by: number;
  
    @Column({
      type: 'datetime',
    })
    updated_at: string;
  
    @Column({
      type: 'int',
    })
    updated_by: number;
    
  }